# calculator/activity_calculator.py
from .activity_enum import ActivityEnum


def max_heart_rate_by_age(age):
    """ Calculates the maximum heart rate at an age.

    Paremeters
    ----------
    age : float
        The age to get the maximum heart rate for

    Returns
    -------
    float 
        The maximum heartrate of an age.
    Raises
    ------

    """
    if age < 0:
        return "Input value is too small"
    elif age > 100:
        return "Input value is too high"
    return 208 - (0.7 * age)


def calculate_heart_rate_percentage(age, percentage):
    """ Calculates the percentage heart rate of the maximum heart rate at the given age.

    Parameters
    ----------
    age : float
        The age to get the maximum heart rate for.
    percentage: float
        The percentage of the maximum heart rate.

    Returns
    -------
    float
        The heart rate at the specified percentage.

    Raises
    ------

    """
    if percentage < 0:
        return "Input value is too small"
    elif percentage > 100:
        return "Input value is too high"
    return max_heart_rate_by_age(age) * percentage / 100


def calculate_activity(heart_rate, age):
    """ Calculates the percentage heart rate of the maximum heart rate at the given age and compares it with the provided heart rate.

    Parameters
    ----------
    heart_rate : float
        The heart rate to analyze.
    age : float
        The age to analyze the heart rate for.

    Returns
    -------
    ActivityEnum
        The type of activity: STAND, MODERATE, INTENSE, HIGH_INTENSE.

    Raises
    ------

    """

    if heart_rate < calculate_heart_rate_percentage(age, 50):
        return ActivityEnum.STAND
    elif heart_rate < calculate_heart_rate_percentage(age, 70):
        return ActivityEnum.MODERATE
    elif heart_rate < calculate_heart_rate_percentage(age, 90):
        return ActivityEnum.INTENSE
    elif heart_rate >= calculate_heart_rate_percentage(age, 90):
        return ActivityEnum.HIGH_INTENSE
