# calculator/activity_enum.py
from enum import Enum


class ActivityEnum(Enum):
    """  
    Atributes
    ---------
    Enum
        The activity types. 

    """
    STAND = "Stand"
    MODERATE = "Moderate"
    INTENSE = "Intense"
    HIGH_INTENSE = "High Intense"
