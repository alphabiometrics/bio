# calculator/heart_rate_resource.py
from flask_restful import Resource

from .activity_calculator import calculate_activity
from .activity_enum import ActivityEnum


class HeartRate(Resource):
    """
    Methods
    -------
    get(heart_rate, age)
        Gets the activity type of heart rate based on age.

    """

    def get(self, heart_rate, age):
        """ Gets the activity type of heart rate based on age.

        Parameters
        ----------
        heart_rate : float
            The heart rate to analyze.
        age : float
            The age to analyze the heart rate for.

        Returns
        -------
        string
            The type of activity along with 200 OK.


        Raises
        ------
        404 Bad Request

        """
        result = calculate_activity(heart_rate, age)
        if isinstance(result, ActivityEnum):
            return result.value, 200

        return "Activity does not exist", 404
