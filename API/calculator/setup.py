# calculator/setup.py
from setuptools import setup

setup(
    name='calculator',
    test_suite='nose.collector',
    tests_require=['nose']
)
