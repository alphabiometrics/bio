
# calculator/test/test_activity_calculator.py
import unittest
from calculator.activity_calculator import max_heart_rate_by_age, calculate_heart_rate_percentage, calculate_activity
from calculator.activity_enum import ActivityEnum

# !!!
# run test from calculator package with command below:
# py setup.py test


class TestCalculator(unittest.TestCase):

    def test_max_heart_rate_by_age(self):
        result = max_heart_rate_by_age(50)
        expected = 173
        self.assertEqual(result, expected)

    def test_max_heart_rate_by_age_low_limit(self):
        result = max_heart_rate_by_age(0)
        expected = 208
        self.assertEqual(result, expected)

    def test_max_heart_rate_by_age_smaller_than_low_limit(self):
        result = max_heart_rate_by_age(-1)
        expected = "Input value is too small"
        self.assertEqual(result, expected)

    def test_max_heart_rate_by_age_high_limit(self):
        result = max_heart_rate_by_age(100)
        expected = 138
        self.assertEqual(result, expected)

    def test_max_heart_rate_by_age_higher_than_high_limit(self):
        result = max_heart_rate_by_age(101)
        expected = "Input value is too high"
        self.assertEqual(result, expected)

    def test_calculate_heart_rate_percentage(self):
        result = calculate_heart_rate_percentage(20, 60)
        expected = 116.4
        self.assertEqual(result, expected)

    def test_calculate_heart_rate_percentage_low_limit(self):
        result = calculate_heart_rate_percentage(20, 0)
        expected = 0
        self.assertEqual(result, expected)

    def test_calculate_heart_rate_percentage_lower_than_low_limit(self):
        result = calculate_heart_rate_percentage(20, -1)
        expected = "Input value is too small"
        self.assertEqual(result, expected)

    def test_calculate_heart_rate_percentage_high_limit(self):
        result = calculate_heart_rate_percentage(20, 100)
        expected = 194
        self.assertEqual(result, expected)

    def test_calculate_heart_rate_percentage_higher_then_high_limit(self):
        result = calculate_heart_rate_percentage(20, 101)
        expected = "Input value is too high"
        self.assertEqual(result, expected)

    # def test_activity_type_stand_low_limit(self):
    #     result = activity_calculator.calculate_activity(0, 20)
    #     expected = activity_enum.ActivityEnum.STAND
    #     self.assertEqual(result, expected)

    def test_activity_type_stand(self):
        result = calculate_activity(60, 20)
        expected = ActivityEnum.STAND
        self.assertEqual(result, expected)

    def test_activity_type_stand_high_limit(self):
        result = calculate_activity(96, 20)
        expected = ActivityEnum.STAND
        self.assertEqual(result, expected)

    def test_activity_type_moderate_low_limit(self):
        result = calculate_activity(97, 20)
        expected = ActivityEnum.MODERATE
        self.assertEqual(result, expected)

    def test_activity_type_moderate(self):
        result = calculate_activity(100, 20)
        expected = ActivityEnum.MODERATE
        self.assertEqual(result, expected)

    def test_activity_type_moderate_high_limit(self):
        result = calculate_activity(135.7, 20)
        expected = ActivityEnum.MODERATE
        self.assertEqual(result, expected)

    def test_activity_type_intense_low_limit(self):
        result = calculate_activity(135.8, 20)
        expected = ActivityEnum.INTENSE
        self.assertEqual(result, expected)

    def test_activity_type_intense(self):
        result = calculate_activity(140, 20)
        expected = ActivityEnum.INTENSE
        self.assertEqual(result, expected)

    def test_activity_type_intense_high_limit(self):
        result = calculate_activity(174.5, 20)
        expected = ActivityEnum.INTENSE
        self.assertEqual(result, expected)

    def test_activity_type_high_intense_low_limit(self):
        result = calculate_activity(174.6, 20)
        expected = ActivityEnum.HIGH_INTENSE
        self.assertEqual(result, expected)

    def test_activity_type_high_intense(self):
        result = calculate_activity(180, 20)
        expected = ActivityEnum.HIGH_INTENSE
        self.assertEqual(result, expected)

#     def test_activity_type_high_intense_high_limit(self):
#         result = activity_calculator.calculate_activity(1000, 20)
#         expected = activity_enum.ActivityEnum.HIGH_INTENSE
#         self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
