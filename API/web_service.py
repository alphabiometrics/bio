# web_service.py
from flask import Flask
from flask_restful import Api
from calculator.heart_rate_resource import HeartRate

# !
# run the service from Api folder (root) with following command:
# py web_service.py

app = Flask(__name__)
api = Api(app)

api.add_resource(HeartRate, "/heartrate/<int:heart_rate>/<int:age>")

if __name__ == "__main__":
    app.run()
# app.run(debug=True)
