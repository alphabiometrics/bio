package com.example.bioapp.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.R;
import com.example.bioapp.firestoreAdapter.FirestoreAdapter;
import com.example.bioapp.firestoreAdapter.IFirestoreAdapter;
import com.example.bioapp.model.Article;
import com.example.bioapp.model.ArticleUsage;
import com.example.bioapp.model.HeartRate;
import com.example.bioapp.model.User;

import java.sql.Date;

public class FullArticleActivity extends AppCompatActivity {

    IFirestoreAdapter firestoreAdapter;

    private final String LAYOUT_TYPE = "normal";
    private Date startDate, endDate;
    private User user;
    Article article;
    HeartRate heartRate;
    ArticleUsage articleUsage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_article);
        firestoreAdapter = new FirestoreAdapter();


        article = (Article) getIntent().getSerializableExtra("articleId");

        TextView title = findViewById(R.id.title);
        title.setText(article.getTitle());

        TextView content = findViewById(R.id.content);
        content.setText(article.getContent());

        TextView author = findViewById(R.id.author);
        author.setText(article.getAuthor());

        ImageView image = findViewById(R.id.image);
        image.setImageResource(article.getImageResource());
    }


    @Override
    protected void onStop() {
        super.onStop();

    }
}
