package com.example.bioapp.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.R;
import com.example.bioapp.ViewController.ActivityController;
import com.example.bioapp.model.Article;

import java.sql.Date;
import java.util.ArrayList;

public class GeneralActivity extends AppCompatActivity implements UserReportDialog.UserReportDialogListener {

    private static final String TAG = "GeneralActivity";
    public static final String SHARED_PREF = "MyPrefsFile";
    private GeneralActivityAdapter generalActivityAdapter;
    private ArrayList<Article> articles;
    private ListView listView;
    private Button workoutButton;
    private ActivityController activityController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        initializeView();
        initializeArticles();
        handleFirstUserLogin();
        handleActivity();
    }

    private void initializeView() {
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.article_list);
        workoutButton = findViewById(R.id.workoutButton);
        workoutButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(GeneralActivity.this, WorkoutActivity.class);
                intent.putExtra("articleId", articles);
                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(GeneralActivity.this, FullArticleActivity.class);
                intent.putExtra("articleId", articles.get(position));
                startActivity(intent);
            }
        });

    }

    private void initializeArticles() {
        activityController = new ActivityController(GeneralActivity.this, getApplicationContext());
        articles = activityController.getArticles();
        generalActivityAdapter = new GeneralActivityAdapter(this, articles);
        listView.setAdapter(generalActivityAdapter);
    }

    private void handleFirstUserLogin() {
        SharedPreferences settings = getSharedPreferences(GeneralActivity.SHARED_PREF, 0);
        boolean hasLoggedIn = settings.getBoolean("hasLoggedIn", false);

        if (!hasLoggedIn) {
            openUserDialog();
        }
    }

    private void handleActivity() {
        activityController.openActivityBasedOnHeartRateWithTimer("articleId", articles);
    }

    private void openUserDialog() {
        UserReportDialog userReportDialog = new UserReportDialog();
        userReportDialog.show(getSupportFragmentManager(), "Android-App");
    }

    @Override
    public void applyInput(String userName, Date birthDate) {
//        Log.d(TAG, "Username is:" + userName + "Birthdate" + birthDate);
    }

    @Override
    protected void onStop() {
        super.onStop();
        activityController.stopHeartRateRequest();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityController.stopHeartRateRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityController.stopHeartRateRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityController.openActivityBasedOnHeartRateWithTimer("articleId", articles);
    }
}

