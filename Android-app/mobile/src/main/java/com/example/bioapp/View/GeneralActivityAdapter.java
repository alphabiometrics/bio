package com.example.bioapp.View;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.R;
import com.example.bioapp.model.Article;

import java.util.ArrayList;

public class GeneralActivityAdapter extends ArrayAdapter<Article> {

    public GeneralActivityAdapter(Activity context, ArrayList<Article> articles) {
        super(context, 0, articles);
    }

    public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.article_list_item, parent, false);
        }
        Article currentArticle = getItem(position);

        TextView title = listItemView.findViewById(R.id.title);
        title.setText(currentArticle.getTitle());

        ImageView image = listItemView.findViewById(R.id.image);
        image.setImageResource(currentArticle.getImageResource());

        return listItemView;
    }
}
