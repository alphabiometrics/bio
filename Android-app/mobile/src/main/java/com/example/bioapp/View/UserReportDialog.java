package com.example.bioapp.View;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.R;
import com.example.bioapp.firestoreAdapter.FirestoreAdapter;
import com.example.bioapp.firestoreAdapter.IFirestoreAdapter;
import com.example.bioapp.model.User;

import java.sql.Date;

public class UserReportDialog extends AppCompatDialogFragment {

    public static final String SHARED_PREF = "MyPrefsFile";
    private EditText editTextUserName;
    private DatePicker birthdayDate;
    private IFirestoreAdapter firestoreAdapter;
    private UserReportDialogListener listener;
    private AlertDialog.Builder builder;

    private String userName;
    private Date userBirthDate;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        firestoreAdapter = new FirestoreAdapter();
        return initializeView();

    }

    public interface UserReportDialogListener {
        void applyInput(String userName, Date birthDate);
    }

    private Dialog initializeView() {
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog, null);

        builder.setView(view)
                .setTitle("Enter your name and birth date")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        storeUserToFirestore();
                        storeUserInternally();
                    }
                });
        editTextUserName = view.findViewById(R.id.userName);
        birthdayDate = view.findViewById(R.id.userBirthday);
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (UserReportDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement UserReportDialogListener");
        }
    }

    private void storeUserToFirestore() {
        userName = editTextUserName.getText().toString();
        userBirthDate = new Date(birthdayDate.getYear(), (birthdayDate.getMonth() + 1), birthdayDate.getDayOfMonth());
        listener.applyInput(userName, userBirthDate);
        firestoreAdapter.addUser(new User(userName, userBirthDate));
    }

    private void storeUserInternally() {
        SharedPreferences settings = getActivity().getSharedPreferences(UserReportDialog.SHARED_PREF, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("UserName", "userName");
        editor.putLong("birthDate", userBirthDate.getTime());
        editor.putBoolean("hasLoggedIn", true);
        editor.apply();
    }
}
