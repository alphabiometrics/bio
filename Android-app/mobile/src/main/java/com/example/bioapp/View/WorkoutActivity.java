package com.example.bioapp.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.R;
import com.example.bioapp.model.Article;

import java.util.ArrayList;

public class WorkoutActivity extends AppCompatActivity {
    public ArrayList<Article> articles;
    private WorkoutAdapter workoutAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);
        initView();

    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        articles = (ArrayList<Article>) getIntent().getSerializableExtra("articleId");
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        workoutAdapter = new WorkoutAdapter(articles);
        recyclerView.setAdapter((workoutAdapter));
    }

    @Override
    protected void onPause() {
        super.onPause();
        workoutAdapter.stopAudio();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        workoutAdapter.stopAudio();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        workoutAdapter.stopAudio();
    }
}
