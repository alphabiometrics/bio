package com.example.bioapp.View;

import android.media.MediaPlayer;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.R;
import com.example.bioapp.model.Article;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.util.Util;
import com.wajahatkarim3.easyflipview.EasyFlipView;
import java.util.ArrayList;
import static android.view.View.VISIBLE;

public class WorkoutAdapter extends RecyclerView.Adapter<WorkoutAdapter.MyViewHolder> {

    private ArrayList<Article> mArticleArrayList;
    private MediaPlayer mediaPlayer;
    // False: audio, True :video, NULL: none
    private Boolean playing;
    private MyViewHolder previousHolder;
    private SimpleExoPlayer exoPlayer;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        EasyFlipView flipView;
        TextView title;
        ImageView backgroundImage;
        ImageButton playAudioButton, playVideoButton;
        PlayerView playerView;
        int currentPosition;


        MyViewHolder(View view) {
            super(view);
            backgroundImage = view.findViewById(R.id.frontBackgroundImage);
            title = view.findViewById(R.id.workoutTitle);
            flipView = view.findViewById(R.id.flipView);
            playAudioButton = view.findViewById(R.id.playAudioButton);
            playVideoButton = view.findViewById(R.id.playVideoButton);
            playing = null;
            playerView = view.findViewById(R.id.playerView);
        }
    }

    public WorkoutAdapter(ArrayList<Article> articlesList) {
        mArticleArrayList = articlesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycle_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.title.setText(mArticleArrayList.get(position).getTitle());
        holder.backgroundImage.setImageResource(mArticleArrayList.get(position).getBackgroundResource());
        holder.currentPosition = position;
        holder.playAudioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (playing == null) {
                    playAudio(view, holder, position);
                    playing = null;
                }
            }
        });

        holder.playVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (playing == null || playing == false) {
                    stopAudio();
                    playVideo(view, holder, position);
                }
            }
        });

        holder.flipView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flipView(view, holder, position);
            }
        });
    }

    private void flipView(View view, MyViewHolder holder, int position) {
        if (mArticleArrayList.get(position).isFlipped) {
            mArticleArrayList.get(position).isFlipped = false;
            stopAudio();
            previousHolder = null;
        } else {
            mArticleArrayList.get(position).isFlipped = true;
            if (previousHolder != null) {
                stopPreviousCard();
            }
            previousHolder = holder;
        }
        flip(holder);
    }

    private void playAudio(View view, MyViewHolder holder, int position) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        mediaPlayer = MediaPlayer.create(view.getContext(), mArticleArrayList.get(position).getAudioResource());
        mediaPlayer.start();
        playing = false;
    }

    private void playVideo(View view, final MyViewHolder holder, int position) {
        holder.playerView.setVisibility(VISIBLE);

        exoPlayer = ExoPlayerFactory.newSimpleInstance(view.getContext(), new DefaultTrackSelector());
        holder.playerView.setPlayer(exoPlayer);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(view.getContext(), Util.getUserAgent(view.getContext(), "Android-app"));
        MediaSource firstSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(RawResourceDataSource.buildRawResourceUri(mArticleArrayList.get(position).getVideoResource()));
        exoPlayer.prepare(firstSource);
        // setting full size of view
        holder.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        exoPlayer.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

        exoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_ENDED) {
                    holder.playerView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
            }

            @Override
            public void onPositionDiscontinuity(int reason) {
            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            @Override
            public void onSeekProcessed() {
            }
        });
        exoPlayer.seekTo(0);
        exoPlayer.setPlayWhenReady(true);
    }

    private void stopPreviousCard() {
        mArticleArrayList.get(previousHolder.currentPosition).isFlipped = false;
        stopAudio();
        stopVideo();
        flip(previousHolder);
        previousHolder = null;
    }

    @Override
    public int getItemCount() {
        return mArticleArrayList.size();
    }

    private void flip(MyViewHolder myViewHolder) {
        myViewHolder.flipView.setFlipDuration(700);
        myViewHolder.flipView.flipTheView();
    }

    public void stopAudio() {
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    public void stopVideo() {
       if(exoPlayer != null) {
           exoPlayer.release();
       }
        previousHolder.playerView.setVisibility(View.INVISIBLE);
    }
}

