package com.example.bioapp.ViewController;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.ArrayAdapter;

import com.example.R;
import com.example.bioapp.View.GeneralActivity;
import com.example.bioapp.View.UserReportDialog;
import com.example.bioapp.model.ActivityTypeEnum;
import com.example.bioapp.View.WorkoutActivity;
import com.example.bioapp.heartRateAPIAdapter.HeartRateApiAdapter;
import com.example.bioapp.model.Article;
import com.example.bioapp.smartWatchAdapter.AndroidSmartWatchAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;


public class ActivityController {

    private final long TIMER_DELAY = 400000;
    private static final String SHARED_PREF = "MyPrefsFile";
    private Activity activity;
    private Context context;
    private AndroidSmartWatchAdapter androidSmartWatchAdapter;
    private HeartRateApiAdapter heartRateApiAdapter;
    String previousActivityType, currentActivityType;

    public ActivityController(Activity activity, Context context) {
        this.activity = activity;
        this.context = context;
        androidSmartWatchAdapter = new AndroidSmartWatchAdapter(activity, context);
        heartRateApiAdapter = new HeartRateApiAdapter();
        previousActivityType = getPreviousActivityTypeFromSharedPreferences();
        currentActivityType = null;
    }

    public synchronized void openActivityBasedOnHeartRateWithTimer(final String extrasName, final ArrayList<?> extrasArrayList) {

        if (currentActivityType == null || !currentActivityType.equals(previousActivityType)) {
            androidSmartWatchAdapter.requestHeartRate();
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    currentActivityType = getCurrentActivityType();
                    androidSmartWatchAdapter.endHeartRateRequest();
                    if (currentActivityType != null || !currentActivityType.equals(previousActivityType)) {
                        previousActivityType = currentActivityType;
                        savePreviousActivityTypeToSharedPreferences();
                        openActivityBasedOnHeartRateAnalysis(extrasName, extrasArrayList);
                    }
                }
            }, TIMER_DELAY);
        }
    }
    private void openActivityWithExtras(final Context context, final Class<?> cls, String extrasName, ArrayList<?> extrasArrayList) {
        Intent intent = new Intent(context, cls);
        intent.putExtra(extrasName, extrasArrayList);
        activity.finish();
        activity.startActivity(intent);
    }

    public void stopHeartRateRequest() {
        androidSmartWatchAdapter.endHeartRateRequest();
    }

    private Class<?> getActivityClassBasedOnHeartRateAnalysis() {
        if (currentActivityType.equals(ActivityTypeEnum.STAND.toString()) || currentActivityType.equals(ActivityTypeEnum.MODERATE.toString())) {
            return GeneralActivity.class;

        } else if (currentActivityType.equals(ActivityTypeEnum.INTENSE.toString()) || currentActivityType.equals(ActivityTypeEnum.HIGH_INTENSE.toString())) {
            return WorkoutActivity.class;
        }
        return null;
    }

    public String getCurrentActivityType() {
        int heartRate = androidSmartWatchAdapter.getHeartRate();
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREF, 0);
        long date = preferences.getLong("birthDate", 0);
        int age = calculateAge(date);
        previousActivityType = currentActivityType;

        return currentActivityType = heartRateApiAdapter.analyseHeartRate(age, heartRate);
    }

    public void openActivityBasedOnHeartRateAnalysis(final String extrasName, final ArrayList<?> extrasArrayList) {
        Class<?> activityClass = getActivityClassBasedOnHeartRateAnalysis();
        openActivityWithExtras(activity, activityClass, extrasName, extrasArrayList);
    }

    private int calculateAge(long dateMillis) {

        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(dateMillis);

        long currentTime = System.currentTimeMillis();
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTimeInMillis(currentTime);

        return currentDate.get(Calendar.YEAR) - date.get(Calendar.YEAR);
    }

    private void savePreviousActivityTypeToSharedPreferences() {
        saveToSharedPreferences("previousActivity", previousActivityType);
    }

    private void saveCurrentActivityTypeToSharedPreferences() {
        saveToSharedPreferences("currentActivityType", currentActivityType);
    }

    private void saveToSharedPreferences(String name, String value) {
        SharedPreferences settings = activity.getSharedPreferences(UserReportDialog.SHARED_PREF, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }

    private String getFromSharedPreferences(String name) {
        SharedPreferences settings = activity.getSharedPreferences(GeneralActivity.SHARED_PREF, 0);
        return settings.getString(name, null);
    }

    private String getPreviousActivityTypeFromSharedPreferences() {
        return getFromSharedPreferences("previousActivity");
    }


    public ArrayList<Article> getArticles() {
        String title = "Huawei Google ban: What does the Google block mean for Huawei phone owners?";
        String content = "Sitting up on a stage, in a large theatre-like room at its sprawling Shenzhen HQ there was much talk from the Huawei executives of America’s rural and \"poorer\" customers who deserve \"equitable access\" to good broadband provision.\n" +
                "\n" +
                "Huawei’s chief legal officer went as far as saying \"connectivity is a basic human right\". Three billion customers across the world are facing the threat of having their welfare \"damaged\" apparently.";
        String content1 = "In the article, she describes meeting Mr Trump in late 1995 or early 1996, in Bergdorf Goodman. She says she recognised him as the \"real estate tycoon\" and that he told her he was buying a present for \"a girl\".\n" +
                "\n" +
                "She says Mr Trump knew she was a TV agony aunt and the two joked around, encouraging each other to try on some lingerie.\n" +
                "\n";
        String content2 = "In the article, she describes meeting Mr Trump in late 1995 or early 1996, in Bergdorf Goodman. She says she recognised him as the \"real estate tycoon\" and that he told her he was buying a present for \"a girl\".\n" +
                "\n" +
                "She says Mr Trump knew she was a TV agony aunt and the two joked around, encouraging each other to try on some lingerie.\n" +
                "\n" +
                "She alleges that they then went to a dressing room, where she accuses him of raping her.";
        String content3 = "Both Mr Trump and Ms Carroll were aged around 50 at the time, and he was married to Marla Maples.\n" +
                "\n" +
                "Ms Carroll says she told two friends about the alleged incident, one of whom advised her to go to the police.\n" +
                "\n" +
                "But she says the other advised her against telling anyone saying: \"Forget it! He has 200 lawyers. He'll bury you.\"";

                ArrayList<Article> articles = new ArrayList<>();
        articles.add(new Article(title, content, "Jacob Jensen", "Politics", R.drawable.news1, R.drawable.background1, R.raw.video1, R.raw.audio1));
        articles.add(new Article("Jet Airways appears to have suspended all its international flights, raising fresh fears about the survival of India's largest private airline.", null, "Gilbert Arenas", "Global", R.drawable.news2, R.drawable.background6, R.raw.video2, R.raw.audio2));
        articles.add(new Article("Uber, the ride-sharing app and delivery business, has warned it \"may not achieve profitability\" as it finally released details of its plan to float.", null, "James Cordon", "Traffic", R.drawable.news3, R.drawable.background7, R.raw.video3, R.raw.audio3));
        articles.add(new Article("Chelsea prevented three people from entering the stadium for Thursday's Europa League quarter-final at Slavia Prague after a video appeared", null, "Charles Barkley", "Sports", R.drawable.news4, R.drawable.background4, R.raw.video4, R.raw.audio4));
        articles.add(new Article("A man with close ties to Wikileaks co-founder Julian Assange has been arrested while trying to leave Ecuador, the country's interior ministry says.", null, "Anton Banders", "Wiki Leaks", R.drawable.news5, R.drawable.background2, R.raw.video1, R.raw.audio5));
        articles.add(new Article("Jet Airways appears to have suspended all its international flights, raising fresh fears about the survival of India's largest private airline. ", null, "Jihnam Azazi", "Politics", R.drawable.news6, R.drawable.background6, R.raw.video2, R.raw.audio6));
        articles.add(new Article("Thursday's coup in Sudan may have seen the overthrow of an unpopular president but those close to Omar al-Bashir are determined to stay in power, writes Sudan expert Alex de Waal.", null, "Walhala ", "Politics", R.drawable.news7, R.drawable.background6, R.raw.video3, R.raw.audio7));
        articles.add(new Article("Thursday's coup in Sudan may have seen the overthrow of an unpopular president but those close to Omar al-Bashir are determined to stay in power, writes Sudan expert Alex de Waal.", null, "Walhala ", "Politics", R.drawable.news7, R.drawable.background3, R.raw.video1, R.raw.audio8));
        return articles;
    }

}
