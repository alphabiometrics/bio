package com.example.bioapp.firestoreAdapter;

import com.example.bioapp.model.Article;
import com.example.bioapp.model.ArticleUsage;
import com.example.bioapp.model.AppUsage;
import com.example.bioapp.model.User;
import com.google.firebase.firestore.FirebaseFirestore;

public class FirestoreAdapter implements IFirestoreAdapter {

    private static FirebaseFirestore db;

    public FirestoreAdapter() {
        db = FirestoreInstanceProvider.getFirestoreInstance();
    }

    @Override
    public void addUser(User user) {
        db.collection("users").add(user);
    }

    @Override
    public void addArticle(Article article) {
        db.collection("article").add(article);
    }

    @Override
    public void addAppUsage(AppUsage appUsage) {
        db.collection("app_usage").add(appUsage);
    }

    @Override
    public void addArticleUsage(ArticleUsage articleUsage) {
        db.collection("article_usage").add(articleUsage);
    }
}
