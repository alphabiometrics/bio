package com.example.bioapp.firestoreAdapter;

import com.google.firebase.firestore.FirebaseFirestore;

class FirestoreInstanceProvider {

    private static FirebaseFirestore dbInstance;

    public static synchronized FirebaseFirestore getFirestoreInstance() {
        if (dbInstance == null) {
            dbInstance = FirebaseFirestore.getInstance();
        }
        return dbInstance;
    }
}


