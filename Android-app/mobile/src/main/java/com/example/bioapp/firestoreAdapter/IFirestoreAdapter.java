package com.example.bioapp.firestoreAdapter;

import com.example.bioapp.model.Article;
import com.example.bioapp.model.ArticleUsage;
import com.example.bioapp.model.AppUsage;
import com.example.bioapp.model.User;

public interface IFirestoreAdapter {

    void addUser(User user);
    void addArticle(Article article);
    void addAppUsage(AppUsage appUsage);
    void addArticleUsage(ArticleUsage articleUsage);

}
