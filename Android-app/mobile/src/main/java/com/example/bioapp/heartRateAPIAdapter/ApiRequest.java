package com.example.bioapp.heartRateAPIAdapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiRequest extends Thread {
    private static final String ONLINE_BASE_URL = "http://bogdanmitrache.pythonanywhere.com/heartrate";
    private int heartRate;
    private int age;
    private boolean running;
    private String output;

    public ApiRequest(int age, int heartrate) {
        this.age = age;
        this.heartRate = heartrate;
        this.running = false;
    }

    public void run() {
        while (running) {
            try {
                String composedUrl = ONLINE_BASE_URL + "/" + heartRate + "/" + age;
                URL url = new URL(composedUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP Error code : "
                            + conn.getResponseCode());
                }
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String temp;
                while ((temp = br.readLine()) != null) {
                    output = temp;
                }
                conn.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }
            cleanOutput();
            running = false;

        }


    }

    private void cleanOutput() {
        if (output != null) {
            String result = output.replace("\"", "");
            output = result;
        }
    }


}
