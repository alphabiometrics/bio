package com.example.bioapp.heartRateAPIAdapter;


import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static java.lang.Thread.sleep;

public class HeartRateApiAdapter {
    private static final long TIMER_DELAY = 5000;
    private static final String ONLINE_BASE_URL = "http://bogdanmitrache.pythonanywhere.com/heartrate";
    private static String output;


    public HeartRateApiAdapter() {

    }

    public synchronized String analyseHeartRate(int age, int heartRate) {
        try {
            int hr = 20;

            String composedUrl = ONLINE_BASE_URL + "/" + heartRate + "/" + hr;
            URL url = new URL(composedUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String temp;
            while ((temp = br.readLine()) != null) {
                output = temp;
            }
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        cleanOutput();
        return output;
    }

    private static void cleanOutput() {
        if (output != null) {
            String result = output.replace("\"", "");
            output = result;
        }
    }
}
