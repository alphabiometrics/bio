package com.example.bioapp.model;

public enum ActivityTypeEnum {

    STAND("Stand"),
    MODERATE("Moderate"),
    INTENSE("Intense"),
    HIGH_INTENSE("High Intense");

    private String stringValue;

     ActivityTypeEnum(String toString) {
        stringValue = toString;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}
