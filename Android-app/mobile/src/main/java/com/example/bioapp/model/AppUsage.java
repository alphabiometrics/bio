package com.example.bioapp.model;

import java.util.Date;

public class AppUsage {

    private String AppUsageId;
    private User user;
    private double heartRate;
    private String heartRateState;
    private Date heartRateDate;
    private String layoutType;
    private Date appOpen, appClosed;

    public AppUsage() {
    }

    public AppUsage(String appUsageId, User user, double heartRate, String heartRateState, Date heartRateDate, String layoutType, Date appOpen, Date appClosed) {
        AppUsageId = appUsageId;
        this.user = user;
        this.heartRate = heartRate;
        this.heartRateState = heartRateState;
        this.heartRateDate = heartRateDate;
        this.layoutType = layoutType;
        this.appOpen = appOpen;
        this.appClosed = appClosed;
    }

    public String getAppUsageId() {
        return AppUsageId;
    }

    public void setAppUsageId(String appUsageId) {
        AppUsageId = appUsageId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(double heartRate) {
        this.heartRate = heartRate;
    }

    public String getHeartRateState() {
        return heartRateState;
    }

    public void setHeartRateState(String heartRateState) {
        this.heartRateState = heartRateState;
    }

    public Date getHeartRateDate() {
        return heartRateDate;
    }

    public void setHeartRateDate(Date heartRateDate) {
        this.heartRateDate = heartRateDate;
    }

    public String getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    public Date getAppOpen() {
        return appOpen;
    }

    public void setAppOpen(Date appOpen) {
        this.appOpen = appOpen;
    }

    public Date getAppClosed() {
        return appClosed;
    }

    public void setAppClosed(Date appClosed) {
        this.appClosed = appClosed;
    }
}
