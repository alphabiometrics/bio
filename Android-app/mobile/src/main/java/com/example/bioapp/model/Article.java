package com.example.bioapp.model;

import java.io.Serializable;

public class Article  implements Serializable {

    public String title;
    public String content;
    public String author;
    public String category;
    public Integer imageResource;
    public Integer backgroundResource;
    public Integer videoResource;
    public Integer audioResource;
    public boolean isFlipped;

    public Article(){
    }

    public Article(String title, String content, String author, String category, Integer imageResource, Integer backgroundResource, Integer videoResource, Integer audioResource) {
        this.title = title;
        this.content = content;
        this.author = author;
        this.category = category;
        this.imageResource = imageResource;
        this.backgroundResource = backgroundResource;
        this.videoResource = videoResource;
        this.audioResource = audioResource;
        this.isFlipped = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getImageResource() {
        return imageResource;
    }

    public void setImageResource(Integer imageResource) {
        this.imageResource = imageResource;
    }

    public Integer getBackgroundResource() {
        return backgroundResource;
    }

    public void setBackgroundResource(Integer backgroundResource) {
        this.backgroundResource= backgroundResource;
    }

    public Integer getVideoResource() {
        return videoResource;
    }

    public void setVideoResource(Integer videoResource) {
        this.videoResource = videoResource;
    }

    public Integer getAudioResource() {
        return audioResource;
    }

    public void setAudioResource(Integer audioResource) {
        this.audioResource = audioResource;
    }
}
