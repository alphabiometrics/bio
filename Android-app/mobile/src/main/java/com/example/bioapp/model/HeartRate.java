package com.example.bioapp.model;

import java.sql.Date;

public class HeartRate {

    private double heartRate;
    private String heartRateState;
    private Date heartRateDate;

    public HeartRate(double heartRate, String heartRateState, Date heartRateDate) {
        this.heartRate = heartRate;
        this.heartRateState = heartRateState;
        this.heartRateDate = heartRateDate;
    }

    public HeartRate(){

    }

    public double getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(double heartRate) {
        this.heartRate = heartRate;
    }

    public String getHeartRateState() {
        return heartRateState;
    }

    public void setHeartRateState(String heartRateState) {
        this.heartRateState = heartRateState;
    }

    public Date getHeartRateDate() {
        return heartRateDate;
    }

    public void setHeartRateDate(Date heartRateDate) {
        this.heartRateDate = heartRateDate;
    }
}
