package com.example.bioapp.model;

import java.sql.Date;

public class User {
    public String name;
    public Date age;

    public User() {
    }

    public User(String name, Date age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAge() {
        return age;
    }

    public void setAge(Date age) {
        this.age = age;
    }
}
