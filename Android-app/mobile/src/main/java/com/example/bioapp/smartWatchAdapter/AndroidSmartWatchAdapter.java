package com.example.bioapp.smartWatchAdapter;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class AndroidSmartWatchAdapter {

    private final String HEART_RATE_REQUEST = "HEART_RATE_REQUEST";
    private final String END_HEART_RATE_REQUEST = "HEART_RATE_STOP";
    private final String DATA_PATH = "/my_path";
    private int heartRate;
    private Activity activity;
    private Context context;
    private IntentFilter messageFilter;
    private Receiver messageReceiver;

    public AndroidSmartWatchAdapter(Activity activity, Context context) {

        this.activity = activity;
        this.context = context;
        heartRate = 0;

        messageFilter = new IntentFilter(Intent.ACTION_SEND);
        messageReceiver = new Receiver();
        LocalBroadcastManager.getInstance(context).registerReceiver(messageReceiver, messageFilter);


    }

    public int getHeartRate() {
        return heartRate;
    }

    public void requestHeartRate() {

        new HeartRateRequest(DATA_PATH, HEART_RATE_REQUEST).start();
    }

    public void endHeartRateRequest() {
        new HeartRateRequest(DATA_PATH, END_HEART_RATE_REQUEST).start();
    }

    public class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            heartRate = intent.getIntExtra("heartRate", 0);
        }
    }

    class HeartRateRequest extends Thread {
        String path;
        String message;

        //Constructor for sending information to the Data Layer//
        HeartRateRequest(String p, String m) {
            path = p;
            message = m;
        }

        public void run() {
            //Retrieve the connected devices, known as nodes
            Task<List<Node>> wearableList =
                    Wearable.getNodeClient(context).getConnectedNodes();
            try {
                List<Node> nodes = Tasks.await(wearableList);
                for (Node node : nodes) {
                    Task<Integer> sendMessageTask =
                            //Send the message
                            Wearable.getMessageClient(activity).sendMessage(node.getId(), path, message.getBytes());
                    try {
                        //Block on a task and get the result synchronously
                        Integer result = Tasks.await(sendMessageTask);

                    } catch (ExecutionException exception) {
                        exception.printStackTrace();
                    } catch (InterruptedException exception) {
                        exception.printStackTrace();
                    }
                }
            } catch (ExecutionException exception) {
                exception.printStackTrace();
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }

}
