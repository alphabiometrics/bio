package com.example.wear;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.R;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.sql.Timestamp;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class MainActivity extends WearableActivity {
    private final String HEART_RATE_REQUEST = "HEART_RATE_REQUEST";
    private final String END_HEART_RATE_REQUEST = "HEART_RATE_STOP";
    private final String DATA_PATH = "/my_path";

    private TextView mTextView;
    private SensorManager mSensorManager;
    private Sensor mHeartRateSensor;
    SensorEventListener sensorEventListener;
    private IntentFilter newFilter;
    private Receiver messageReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        initView();
        checkPermissions(this, this);
        initSensor();
        registerToReceiveLocalBroadcasts();
    }

    private void initView() {
        setContentView(R.layout.activity_main);
        mTextView = (TextView) findViewById(R.id.text);
        mTextView.setText("Sensor Stopped");
        // Enables Always-on
        setAmbientEnabled();
    }

    private void registerToReceiveLocalBroadcasts() {
        newFilter = new IntentFilter(Intent.ACTION_SEND);
        messageReceiver = new Receiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, newFilter);
    }

    private void initSensor() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);

        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                float mHeartRateFloat = event.values[0];
                long mills = event.timestamp;
                Timestamp ts = new Timestamp(mills);
                int heartRate = Math.round(mHeartRateFloat);
                displayHeartRate(heartRate);
                sendHeartRate(heartRate);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
    }

    private void startSensor() {
        mSensorManager.registerListener(sensorEventListener, mHeartRateSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    private void stopSensor() {
        mSensorManager.unregisterListener(sensorEventListener);
        mTextView.setText("Sensor Stopped");
    }

    private void checkPermissions(Context context, Activity activity) {

        boolean hasBodySensorPermission =
                ContextCompat.checkSelfPermission(context, Manifest.permission.BODY_SENSORS) == PackageManager.PERMISSION_GRANTED;

        if (!hasBodySensorPermission) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.BODY_SENSORS},
                    100);
        }
    }

    private void sendHeartRate(int heartRate) {
        new HeartRateResponse(DATA_PATH, heartRate + "").start();
    }

    private void displayHeartRate(int heartRate) {
        mTextView.setText(heartRate + "\tBPM");
    }

    public class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            intent.getStringExtra("message");
            String receivedMessage = intent.getStringExtra("message");

            if (receivedMessage.equals(HEART_RATE_REQUEST)) {
                startSensor();
            } else if (receivedMessage.equals(END_HEART_RATE_REQUEST)) {
                stopSensor();
            }
        }
    }

    class HeartRateResponse extends Thread {
        String path;
        String message;

        //Constructor for sending information to the Data Layer//
        HeartRateResponse(String p, String m) {
            path = p;
            message = m;
        }

        public void run() {
            //Retrieve the connected devices//
            Task<List<Node>> nodeListTask =
                    Wearable.getNodeClient(getApplicationContext()).getConnectedNodes();
            try {
                //Block on a task and get the result synchronously
                List<Node> nodes = Tasks.await(nodeListTask);
                for (Node node : nodes) {

                    //Send the message///
                    Task<Integer> sendMessageTask =
                            Wearable.getMessageClient(MainActivity.this).sendMessage(node.getId(), path, message.getBytes());

                    try {
                        Integer result = Tasks.await(sendMessageTask);

                    } catch (ExecutionException exception) {
                        exception.printStackTrace();
                    } catch (InterruptedException exception) {
                        exception.printStackTrace();
                    }
                }
            } catch (ExecutionException exception) {
                exception.printStackTrace();
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        stopSensor();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopSensor();
    }

}
