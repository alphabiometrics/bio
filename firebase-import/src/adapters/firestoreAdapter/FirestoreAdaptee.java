package adapters.firestoreAdapter;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FirestoreAdaptee {

    private HttpURLConnection httpURLConnection;

    public FirestoreAdaptee() {

    }


    public JSONObject getRequest(String url) {
        try {
            URL temp = new URL(url);
            httpURLConnection = (HttpURLConnection) temp.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setReadTimeout(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response();

    }

    private JSONObject response() {

        String line = null;
        BufferedReader reader = null;
        StringBuffer responseContent = null;
        try {
            int status = httpURLConnection.getResponseCode();
            if (status != 200) {
                reader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }
                reader.close();
            } else {
                reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                while ((line = reader.readLine()) != null) {

                    responseContent.append(line);

                }
                reader.close();
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return new JSONObject(responseContent.toString());
    }
}
