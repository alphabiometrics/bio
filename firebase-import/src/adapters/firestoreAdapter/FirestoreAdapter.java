package adapters.firestoreAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

public class FirestoreAdapter implements IFirestoreAdapter {

    private FirestoreAdaptee firestoreAdaptee;

    public FirestoreAdapter() {
        firestoreAdaptee = new FirestoreAdaptee();
    }


    @Override
    public JSONArray getUsers() {
        JSONObject obj = firestoreAdaptee.getRequest("https://firestore.googleapis" +
                ".com/v1/projects/biotest-38a6c/databases/" +
                "(default)/documents/users");
        return obj.getJSONArray("name");
    }

    @Override
    public JSONArray getArticles() {
        JSONObject obj = firestoreAdaptee.getRequest("https://firestore.googleapis" +
                ".com/v1/projects/biotest-38a6c/databases/" +
                "(default)/documents/article");
        return obj.getJSONArray("name");
    }

    @Override
    public JSONArray getAppUsage() {
        JSONObject obj = firestoreAdaptee.getRequest("https://firestore.googleapis" +
                ".com/v1/projects/biotest-38a6c/databases/" +
                "(default)/documents/app_usage");
        return obj.getJSONArray("name");
    }

    @Override
    public JSONArray getArticleUsage() {
        JSONObject obj = firestoreAdaptee.getRequest("https://firestore.googleapis" +
                ".com/v1/projects/biotest-38a6c/databases/" +
                "(default)/documents/article_usage");
        return obj.getJSONArray("name");
    }
}
