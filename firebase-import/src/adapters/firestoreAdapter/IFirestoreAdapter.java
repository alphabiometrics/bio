package adapters.firestoreAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IFirestoreAdapter {


    JSONArray getUsers();
    JSONArray getArticles();
    JSONArray getAppUsage();
    JSONArray getArticleUsage();
}
