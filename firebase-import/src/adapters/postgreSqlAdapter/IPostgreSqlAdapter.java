package adapters.postgreSqlAdapter;


import java.sql.Date;

public interface IPostgreSqlAdapter<T> {


    void insertAppUsage(String idusage, String userId, String userName, java.sql.Date birthdate, double heartRate, String heartRateState,
                        java.sql.Date heartRateDate, String layoutType, java.sql.Date appOpen, java.sql.Date appClosed);

    void insertUser(String userId, String userName, java.sql.Date birthDate);

    void insertArticle(String article_id, String title, String content, String author, String video, String audio, String category, java.sql.Date published);

    void insertArticleUsage(String article_usage_id, String userId, String userName, java.sql.Date birthday,
                            String articleId, String articleTitle,
                            String content, String author, String category, java.sql.Date published,
                            String video, String audio, java.sql.Date openDate, java.sql.Date closedDate, double heartRate,
                            String heartRateState, Date heartRateDate, String layoutType);

    void createPostgreSqlTables();

}
