package adapters.postgreSqlAdapter;

import java.sql.Date;

public class PostgreSqlAdapter implements IPostgreSqlAdapter {


    private PostgresSQLAdaptee postgresSQLAdaptee;


    public PostgreSqlAdapter() {
        try {
            postgresSQLAdaptee = new PostgresSQLAdaptee();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertAppUsage(String idusage, String userId, String userName, Date birthdate, double heartRate, String heartRateState,
                               Date heartRateDate, String layoutType, Date appOpen, Date appClosed) {

        String sql = "insert into public." + Table.APP_USAGE + "(app_usage_id,user_id,user_name,birthday,heart_rate,heart_rate_state,heart_rate_date," +
                "layout_type,app_open,app_closed)" +
                " values ('" +
                idusage + "','" +
                userId + "','" +
                userName + "','" +
                birthdate + "','" +
                heartRate + "','" +
                heartRateState + "','" +
                heartRateDate + "','" +
                layoutType + "','" +
                appOpen + "','" +
                appClosed +
                "')";
        postgresSQLAdaptee.update(sql);

    }

    @Override
    public void insertUser(String userId, String userName, Date birthDate) {

        String sql = "insert into public." + Table.USERS + "(user_id, user_name, birthday)" +
                " values ('" +
                userId + "','" +
                userName + "','" +
                birthDate +
                "')";
        postgresSQLAdaptee.update(sql);

    }

    @Override
    public void insertArticle(String article_id, String title, String content, String author, String video, String audio, String category, Date published) {

        String sql = "insert into public." + Table.ARTICLE + "(article_id, title, content, author, video, audio, category, published)" +
                " values ('" +
                article_id + "','" +
                title + "','" +
                content + "','" +
                author + "','" +
                video + "','" +
                audio + "','" +
                category + "','" +
                published +
                "')";
        postgresSQLAdaptee.update(sql);

    }

    @Override
    public void insertArticleUsage(String article_usage_id, String userId, String userName, Date birthday,
                                   String articleId, String articleTitle,
                                   String content, String author, String category, Date published,
                                   String video, String audio, Date openDate, Date closedDate, double heartRate,
                                   String heartRateState, Date heartRateDate, String layoutType) {

        String sql = "insert into public." + Table.ARTICLE_USAGE +
                "(article_usage_id, user_id, user_name, birthday,article_id, article_title, article_content, " +
                "article_author, article_category, article_published, article_video, article_audio, " +
                "article_open, article_closed, heart_rate, heart_rate_state, heart_rate_date, layout_type)" +
                " values ('" +
                article_usage_id + "','" +
                userId + "','" +
                userName + "','" +
                birthday + "','" +
                articleId + "','" +
                articleTitle + "','" +
                content + "','" +
                author + "','" +
                category + "','" +
                published + "','" +
                video + "','" +
                audio + "','" +
                openDate + "','" +
                closedDate + "','" +
                heartRate + "','" +
                heartRateState + "','" +
                heartRateDate + "','" +
                layoutType +
                "')";
        postgresSQLAdaptee.update(sql);

    }

    @Override
    public void createPostgreSqlTables() {
        TableCreator.createUserTable(postgresSQLAdaptee);
        TableCreator.createArticleTable(postgresSQLAdaptee);
        TableCreator.createArticleUsageTable(postgresSQLAdaptee);
        TableCreator.createAppUsageTable(postgresSQLAdaptee);
    }
}