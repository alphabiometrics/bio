package adapters.postgreSqlAdapter;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;


public class PostgresSQLAdaptee {
    private String url;
    private String user;
    private String pw;
    private Connection connection;

    private static final String DRIVER = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://localhost:5432/biometrics";
    private static final String USER = "postgres";
    private static final String PASSWORD = "postgres";

    public PostgresSQLAdaptee(String driver, String url, String user, String pw)
            throws ClassNotFoundException {
        this.url = url;
        this.user = user;
        this.pw = pw;
        connection = null;
        Class.forName(driver);
    }

    public PostgresSQLAdaptee(String databaseName, String user, String pw)
            throws ClassNotFoundException {
        this(DRIVER, URL + databaseName, user, pw);
    }

    public PostgresSQLAdaptee(String databaseName) throws ClassNotFoundException {
        this(DRIVER, URL + databaseName, USER, PASSWORD);
    }

    public PostgresSQLAdaptee() throws ClassNotFoundException {
        this(DRIVER, URL, USER, PASSWORD);
    }

    private void openDatabase() {
        try{
            connection = DriverManager.getConnection(url, user, pw);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void closeDatabase()  {
       try{
           connection.close();
       }
       catch (Exception e){
           e.printStackTrace();
       }
    }

    public void update(String sql) {
        try {
            openDatabase();
            Statement stm = connection.createStatement();
            stm.executeUpdate(sql);
            closeDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ResultSet query(String sql)  {
        ResultSet rs = null;
        try {
            openDatabase();
            Statement stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);
            closeDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return rs;
    }

}
