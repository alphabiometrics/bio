package adapters.postgreSqlAdapter;

public enum Table {
    USERS ("users"),
    ARTICLE("article"),
    APP_USAGE("app_usage"),
    ARTICLE_USAGE("article_usage");



    private String val;

    Table(String str){
        val = str;
    }

    @Override
    public String toString(){
        return  val;
    }
}
