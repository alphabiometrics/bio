package adapters.postgreSqlAdapter;

public class TableCreator {


    public static void createAppUsageTable(PostgresSQLAdaptee postgresSQLAdaptee) {
        String sql = "create table " + Table.APP_USAGE + " (" +
                "app_usage_id VARCHAR(50)," +
                "user_id VARCHAR(50)," +
                "user_name VARCHAR(50)," +
                "birthday DATE," +
                "heart_rate REAL," +
                "heart_rate_state VARCHAR(50)," +
                "heart_rate_date DATE," +
                "layout_type VARCHAR(50)," +
                "app_open DATE," +
                "app_closed DATE" +
                ");";

        postgresSQLAdaptee.update(sql);

    }

    public static void createArticleUsageTable(PostgresSQLAdaptee postgresSQLAdaptee) {
        String sql = "create table " + Table.ARTICLE_USAGE + " (" +
                "article_usage_id VARCHAR(50)," +
                "user_id VARCHAR(50)," +
                "user_name VARCHAR(50)," +
                "birthday DATE," +
                "article_id VARCHAR(50)," +
                "article_title VARCHAR(50)," +
                "article_content VARCHAR(50)," +
                "article_author VARCHAR(50)," +
                "article_category VARCHAR(50)," +
                "article_published VARCHAR(50)," +
                "article_video VARCHAR(50)," +
                "article_audio VARCHAR(50)," +
                "article_open DATE," +
                "article_closed DATE," +
                "heart_rate REAL," +
                "heart_rate_state VARCHAR(50)," +
                "heart_rate_date DATE," +
                "layout_type VARCHAR(50)" +
                ");";
        postgresSQLAdaptee.update(sql);

    }

    public static void createUserTable(PostgresSQLAdaptee postgresSQLAdaptee) {
        String sql = "create table " + Table.USERS + "(" +
                "user_id VARCHAR(50)," +
                "user_name VARCHAR(50)," +
                "birthday DATE" +
                ");";

        postgresSQLAdaptee.update(sql);
    }

    public static void createArticleTable(PostgresSQLAdaptee postgresSQLAdaptee) {
        String sql = "create table " + Table.ARTICLE + " (" +
                "article_id VARCHAR(50)," +
                "title VARCHAR(50)," +
                "content VARCHAR(5000)," +
                "author VARCHAR(50)," +
                "video VARCHAR(50)," +
                "audio VARCHAR(50)," +
                "category VARCHAR(50)," +
                "published DATE" +
                ");";

        postgresSQLAdaptee.update(sql);
    }
}
