package controllers;

import model.*;
import org.json.JSONObject;

import java.util.ArrayList;

public class Controller implements IController {
    IFirestoreController firestoreController;
    IPostgresSQLController postgresSQLController;


    public Controller() {
        firestoreController = new FirestoreController();
        postgresSQLController = new PostgreSqlController();
    }


    @Override
    public void importUsers() {
        ArrayList<User> users = firestoreController.getUsers();
        postgresSQLController.insertUsers(users);
    }

    @Override
    public void importArticles() {
        ArrayList<Article> articles = firestoreController.getArticles();
        postgresSQLController.insertArticles(articles);
    }

    @Override
    public void importArticleUsage() {
        ArrayList<ArticleUsage> articleUsages = firestoreController.getArticleUsages();
        postgresSQLController.insertArticleUsages(articleUsages);
    }

    @Override
    public void importAppUsage() {
        ArrayList<AppUsage> appUsages = firestoreController.getAppUsages();
        postgresSQLController.insertAppUsages(appUsages);
    }

    @Override
    public void createPostgresSqlTables() {

    }

    private void fromJsonToUser(JSONObject jsonObject) {

    }


}
