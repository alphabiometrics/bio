package controllers;

import adapters.firestoreAdapter.FirestoreAdapter;
import adapters.firestoreAdapter.IFirestoreAdapter;
import model.AppUsage;
import model.Article;
import model.ArticleUsage;
import model.User;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;

public class FirestoreController implements IFirestoreController {

    IFirestoreAdapter firestoreAdapter;

    public FirestoreController() {
        firestoreAdapter = new FirestoreAdapter();
    }


    @Override
    public ArrayList<User> getUsers() {

        ArrayList<User> users = new ArrayList<>();
        JSONArray array = firestoreAdapter.getUsers();
        JSONObject temp = null;
        for (Object obj : array) {
            temp = (JSONObject) obj;

            User usr = new User(temp.getString("name"), new Date(temp.get("birthDate")));
            users.add(usr);
        }
        return users;
    }

    @Override
    public ArrayList<Article> getArticles() {
        ArrayList<Article> articles = new ArrayList<>();
        JSONArray array = firestoreAdapter.getArticles();
        JSONObject temp = null;
        for (Object obj : array) {
            temp = (JSONObject) obj;
            Article art = new Article(temp.getString("title"), temp.getString("content"), temp.getString("author"),
                    temp.getString("category"), temp.getString("videoresource"),
                    temp.getString("videoresource"), new Date(temp.getString("publishedDate")));

            articles.add(art);
        }
        return articles;
    }

    @Override
    public ArrayList<AppUsage> getAppUsages() {
        return null;
    }

    @Override
    public ArrayList<ArticleUsage> getArticleUsages() {
        ArrayList<ArticleUsage> articleUsages = new ArrayList<>();
        JSONArray array = firestoreAdapter.getArticleUsage();
        JSONObject temp = null;
        for (Object obj : array) {
            temp = (JSONObject) obj;
            ArticleUsage usage = new ArticleUsage(
                    temp.getString("title"),
                    temp.getString("content"),
                    temp.getString("author"),
                    temp.getString("category"),
                    temp.getString("videoresource"),
                    temp.getString("videoresource"),
                    new Date(temp.getString("publishedDate")));

            articleUsages.add(usage);
        }
        return articleUsages;

    }
}
