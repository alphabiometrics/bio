package controllers;

public interface IController {

    void importUsers();

    void importArticles();

    void importArticleUsage();

    void importAppUsage();

    void createPostgresSqlTables();
}
