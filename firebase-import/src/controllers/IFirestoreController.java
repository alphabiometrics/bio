package controllers;

import model.*;

import java.util.ArrayList;

public interface IFirestoreController {


    ArrayList<User> getUsers();

    ArrayList<Article> getArticles();

    ArrayList<AppUsage> getAppUsages();

    ArrayList<ArticleUsage> getArticleUsages();

}
