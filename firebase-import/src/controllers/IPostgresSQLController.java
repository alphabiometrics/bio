package controllers;

import model.AppUsage;
import model.Article;
import model.ArticleUsage;
import model.User;

import java.util.ArrayList;

public interface IPostgresSQLController {

    void insertUsers(ArrayList<User> users);

    void insertArticles(ArrayList<Article> articles);

    void insertAppUsages(ArrayList<AppUsage> appUsages);

    void insertArticleUsages(ArrayList<ArticleUsage> articleUsages);
}
