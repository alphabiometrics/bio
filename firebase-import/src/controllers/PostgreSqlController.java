package controllers;

import adapters.postgreSqlAdapter.IPostgreSqlAdapter;
import adapters.postgreSqlAdapter.PostgreSqlAdapter;
import model.AppUsage;
import model.Article;
import model.ArticleUsage;
import model.User;

import java.util.ArrayList;

public class PostgreSqlController implements IPostgresSQLController {

    private IPostgreSqlAdapter postgresSQLAdaptor;

    public PostgreSqlController() {
        postgresSQLAdaptor = new PostgreSqlAdapter();
    }

    @Override
    public void insertUsers(ArrayList<User> users) {
        for (User user : users) {
            postgresSQLAdaptor.insertUser(user.getId(), user.getName(), user.getBirthDate());
        }
    }

    @Override
    public void insertArticles(ArrayList<Article> articles) {
        for (Article article : articles) {
            postgresSQLAdaptor.insertArticle(article.getId(), article.getTitle(), article.getContent(),
                    article.getAuthor(), article.getVideoResource(), article.getAudioResource(), article.getCategory(),
                    article.getPublishDate());
        }
    }

    @Override
    public void insertAppUsages(ArrayList<AppUsage> appUsages) {
        for (AppUsage appUsage : appUsages) {
            postgresSQLAdaptor.insertAppUsage(appUsage.getId(), appUsage.getUser().getId(),
                    appUsage.getUser().getName(), appUsage.getUser().getBirthDate(), appUsage.getHeartRate(),
                    appUsage.getHeartRateState(), appUsage.getHeartRateDate(), appUsage.getLayoutType(),
                    appUsage.getAppOpen(), appUsage.getAppClosed());
        }
    }

    @Override
    public void insertArticleUsages(ArrayList<ArticleUsage> articleUsages) {
        for (ArticleUsage articleUsage : articleUsages) {
            postgresSQLAdaptor.insertArticleUsage(
                    articleUsage.getId(),
                    articleUsage.getUser().getId(),
                    articleUsage.getUser().getName(),
                    articleUsage.getUser().getBirthDate(),
                    articleUsage.getArticle().getId(),
                    articleUsage.getArticle().getTitle(),
                    articleUsage.getArticle().getContent(),
                    articleUsage.getArticle().getAuthor(),
                    articleUsage.getArticle().getCategory(),
                    articleUsage.getArticle().getPublishDate(),
                    articleUsage.getArticle().getVideoResource(),
                    articleUsage.getArticle().getAudioResource(),
                    articleUsage.getOpenDate(),
                    articleUsage.getCloseDate(),
                    articleUsage.getHeartRate(),
                    articleUsage.getHeartRateState(),
                    articleUsage.getHeartRateDate(),
                    articleUsage.getLayoutType());
        }
    }
}
