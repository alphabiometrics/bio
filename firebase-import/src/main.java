import controllers.Controller;
import controllers.IController;

public class main {


    public static void main(String[] args) {

        IController controller = new Controller();
        
        controller.importUsers();
        controller.importArticles();
        controller.importArticleUsage();
        controller.importAppUsage();


    }
}
