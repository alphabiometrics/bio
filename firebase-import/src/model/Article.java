package model;

import java.io.Serializable;
import java.sql.Date;

public class Article implements Serializable {

    private String id;
    private String title;
    private String content;
    private String author;
    private String category;
    private String videoResource;
    private String audioResource;
    private boolean isFlipped;
    private Date publishDate;

    public Article() {
    }

    public Article(String title, String content, String author, String category,
                   String videoResource, String audioResource, Date publishDate) {
        this.id = null;
        this.title = title;
        this.content = content;
        this.author = author;
        this.category = category;
        this.videoResource = videoResource;
        this.audioResource = audioResource;
        this.publishDate = publishDate;
        this.isFlipped = false;
    }

    public Article(String id, String title, String content, String author, String category, String videoResource, String audioResource, Date publishDate) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.author = author;
        this.category = category;
        this.videoResource = videoResource;
        this.audioResource = audioResource;
        this.publishDate = publishDate;
        this.isFlipped = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getVideoResource() {
        return videoResource;
    }

    public void setVideoResource(String videoResource) {
        this.videoResource = videoResource;
    }

    public String getAudioResource() {
        return audioResource;
    }

    public void setAudioResource(String audioResource) {
        this.audioResource = audioResource;
    }


    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }
}
