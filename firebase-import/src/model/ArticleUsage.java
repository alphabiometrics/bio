package model;

import java.sql.Date;

public class ArticleUsage {

    private String id;
    private User user;
    private Article article;
    private Date openDate, closeDate;
    private double heartRate;
    private String heartRateState;
    private Date heartRateDate;
    private String layoutType;

    public ArticleUsage() {
    }

    public ArticleUsage(String id, User user, Article article, Date openDate, Date closeDate, double heartRate, String heartRateState, Date heartRateDate, String layoutType) {
        this.id = id;
        this.user = user;
        this.article = article;
        this.openDate = openDate;
        this.closeDate = closeDate;
        this.heartRate = heartRate;
        this.heartRateState = heartRateState;
        this.heartRateDate = heartRateDate;
        this.layoutType = layoutType;
    }
    public ArticleUsage( User user, Article article, Date openDate, Date closeDate, double heartRate, String heartRateState, Date heartRateDate, String layoutType) {
        this.user = user;
        this.article = article;
        this.openDate = openDate;
        this.closeDate = closeDate;
        this.heartRate = heartRate;
        this.heartRateState = heartRateState;
        this.heartRateDate = heartRateDate;
        this.layoutType = layoutType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public double getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(double heartRate) {
        this.heartRate = heartRate;
    }

    public String getHeartRateState() {
        return heartRateState;
    }

    public void setHeartRateState(String heartRateState) {
        this.heartRateState = heartRateState;
    }

    public Date getHeartRateDate() {
        return heartRateDate;
    }

    public void setHeartRateDate(Date heartRateDate) {
        this.heartRateDate = heartRateDate;
    }

    public String getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }
}
